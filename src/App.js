import React,  { Suspense }  from 'react';
import ErrorBoundary from "./ErrorBoundary";
const Mfe1App = React.lazy(() => import("mfe1/App"));
const Mfe2App = React.lazy(() => import("mfe2/App"));

const RemoteWrapper = ({ children }) => (
  <div
    style={{
      border: "1px solid red",
      background: "white",
    }}
  >
    <ErrorBoundary>{children}</ErrorBoundary>
  </div>
);

function App(){
    return (
        <div>
            <h1 className="container">Header from Container</h1>
            <Suspense fallback={<div>Loading... </div>}>
            <RemoteWrapper>
              <Mfe1App />
            </RemoteWrapper>
            <RemoteWrapper>
              <Mfe2App />
            </RemoteWrapper>
            </Suspense>     
        </div>
    );
}

export default App;