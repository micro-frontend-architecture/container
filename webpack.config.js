const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const { dependencies } = require("./package.json");

module.exports = {
    mode: 'development',
    devServer: {
      port:5000,
      compress: true,
      allowedHosts: "all",  
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-react', '@babel/preset-env'],
                    plugins: ['@babel/plugin-transform-runtime'],
                  },
                },
              },
              {
                test: /\.?css$/,
                use: [
                  'style-loader',
                  'css-loader',
                  'sass-loader'
                ]
              }
        ]
    },
    plugins: [
        new ModuleFederationPlugin({
          name: 'container',
          remotes: {
            mfe1: `Mfe1Remote@http://localhost:5001/moduleEntry.js`,
            mfe2: `Mfe2Remote@http://localhost:5002/moduleEntry.js`
          },
          shared:{
            ...dependencies,
            react: {
              singleton: true,
              requiredVersion: dependencies["react"],
            },
            "react-dom": {
              singleton: true,
              requiredVersion: dependencies["react-dom"],
            }
          }
        }),
        new HtmlWebpackPlugin({
          template: './public/index.html',
        }),
      ]    
  };